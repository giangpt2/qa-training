package com.vhades.qatraining;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QaTrainingApplication {

	public static void main(String[] args) {
		SpringApplication.run(QaTrainingApplication.class, args);
	}

}
